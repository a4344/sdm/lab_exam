const {query} = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");


router.get("/:movie_title", (request, response) => {
    const { movie_title } = request.params;
    
    const connection = utils.openConnection();

    const statement = `
    select * from Movie where
    movie_title = '${movie_title}
    `;

    connection.query(statement, (error, result) => {
        connection.end();
        if (result.length > 0) {
            console.log(result.length);
            console.log(result);
            response.send(utils.createResult(error, result));
        }else{
            response.send("user not found!!!!");
        }
    });
});

router.post("/add", (request, response) => {
    const { movie_title,movie_release_date,movie_director } = request.body;

    const connection = utils.openConnection();
    console.log(connection);
    const statement = `
    insert into Movie
    ( movie_title,movie_release_date,movie_director)
    values
    ( '${movie_title},${movie_release_date},${movie_director})
    `;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResult(error, result));
    });
});

router.put("/update/:movie_title", (request, response) => {
    const { movie_release_date } = request.params;
    const { movie_time } = request.body;

    const statement = `
    update Movie
    set
    movie_release_date = $ {movie_release_date}
    movie_time = $ {movie_time}
    where
    movie_title ='${movie_title}'
    `;
    const connection = utils.openConnection();
    connection.query(statement, (error, result) => {
        connection.end();
        console.log(statement);

        response.send(utils.createResult(error. result));
    });


});

router.delete("/remove/:movie_title", (request, response) => {
const {movie_title } = request.params;
const statement = `
delete from Movie
where
movie_title = $ {movie_title}
`;
const connection = utils.openConnection();
connection.query(statement, (error, result) => {
    connection.end();
        console.log(statement);

        response.send(utils.createResult(error. result));
    });


});





