create table Movie(movie_id integer primary key auto_increment,
 movie_title varchar(200),
 movie_release_date date,
 movie_time timestamp default CURRENT_TIMESTAMP,
 movie_director varchar(200)
 );